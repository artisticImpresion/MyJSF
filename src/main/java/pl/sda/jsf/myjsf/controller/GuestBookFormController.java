/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.jsf.myjsf.controller;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author artisticImpresion
 */
@ManagedBean(name = "form")//nazwa, która wskazuje czego dotyczy ten bean - tutaj formularz z GuestBook
@RequestScoped
public class GuestBookFormController {
    private String nick;
    private String entry;

    
    @ManagedProperty(value = "#{applicationController}")
    private ApplicationController applicationController;
    
    public ApplicationController getApplicationController() {
        return applicationController;
    }
    
    public void setApplicationController(ApplicationController applicationController) {
        this.applicationController = applicationController;
    }
        
    /**
     * Creates a new instance of GuestBookFormController
     */
    public GuestBookFormController() {
    }
    
    public String getNick(){
        return nick;
    }
    
    public void setNick(String nick) {
        this.nick = nick;
    }
    
    public String getEntry() {
        return entry;
    }

    public void setEntry(String nick) {
        this.entry = entry;
    }
    
    public void save() {
        this.setNick("SET!");

        EntityManager em = applicationController.getEntityManager();

        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        //your code here
        transaction.commit();
        
        System.out.println("dopisano do bazy");
        
    }
    
}
