/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.jsf.myjsf.controller;

//import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

/**
 *
 * @author artisticImpresion
 */
//@Named(value = "aboutMe")
@ManagedBean(name = "aboutMe")
@RequestScoped
public class AboutMe {

    /**
     * Creates a new instance of AboutMe
     */
    public AboutMe() {
    }
 
    //W to miejsce wstrzykuję instancję kontrolera - nie muszę tworzyć nowego...
    @ManagedProperty(value = "#{applicationController}")
    private ApplicationController applicationController;
    
    public ApplicationController getApplicationController(){
        return applicationController;
    }
    
    
}
