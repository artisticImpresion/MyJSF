/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.jsf.myjsf.controller;

import java.util.List;
import pl.sda.jsf.myjsf.model.GuestBook;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author artisticImpresion
 */
@ManagedBean(name = "applicationController") //@Named(value wymieniamy na ManagedBean
@ApplicationScoped
public class ApplicationController {
    
    private String version = "0.4.2.beta";
    
    private final EntityManagerFactory emf;
    private final EntityManager em;

    /**
     * Creates a new instance of ApplicationController
     */
    public ApplicationController() {
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }
    
    public EntityManager getEntityManager(){
        return em;
    }

    public String getVersion() {
        return version;
    }
    
    public List<GuestBook> getList() {
        Query query = em.createQuery("from pl.sda.jsf.myjsf.model.GuestBook nick, entry");
        return query.getResultList();
    }
    
}
